for %%* in (.) do set CurrDirName=%%~nx*
docker kill %CurrDirName%
docker rm %CurrDirName%
docker run -ti --name %CurrDirName% -p 8000:80 -v %~dp0/src:/src^
 -e API_KEY=""^
 -e RESOURCE_AMBASSADOR="http://lb.internal.matrix-computer.com/resource-ambassador"^
 -e RESOURCE_AUTHORIZATION="http://lb.internal.matrix-computer.com/upline-authorization-read"^
 python/%CurrDirName% python -m unittest discover /src/tests