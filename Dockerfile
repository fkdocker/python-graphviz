FROM alpine:3.4

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache \
	graphviz python3 py-pip py-flask


# install pip modules (own filesystem layer)
COPY src/app/requirements.txt /src/app/requirements.txt
RUN pip3 install -r /src/app/requirements.txt

# copy our app
COPY src /src


# some info for the callers
EXPOSE 80
VOLUME ["/src"]
WORKDIR "/src/app"
# run our service
CMD ["python3","/src/app/main.py"]

