from flask import Flask, jsonify, request, redirect, url_for
import subprocess
from tempfile import NamedTemporaryFile as ntf

app = Flask(__name__)

@app.route('/birdview.svg', methods=['GET'])
def default():
    print("here we go")
    suffix = "svg"
    with ntf(suffix=".dot", mode="w") as f_dot, ntf(mode="r+b") as f_out, ntf(mode="r") as f_err:
        cmd = [
                'dot',
                '-T',
                suffix,
                'birdview.dot',
                '-o',
                f_out.name
        ]

        proc = subprocess.Popen(cmd, stdout=f_err, stderr=subprocess.STDOUT)
        ret = proc.wait()

        if ret != 0:
            f_err.seek(0)
            response = jsonify({"status_code": ret, "message": f_err.read()})
            response.status_code = 500
            
            return response

        f_out.seek(0)
        out_data = f_out.read()

        return out_data


if __name__ == '__main__':
    app.run(debug=True, port=8000)