from lib import lib
from os import environ
from flask import request
import requests


lib.selfcheck()

ambassador_version = 'v1'

if "RESOURCE_AMBASSADOR" in environ:
    ambassador_url = environ["RESOURCE_AMBASSADOR"]
    print("Using Ambassador: " + ambassador_url)
else: 
    print("Ambassador environment-variable not set!")

if "API_KEY" in environ:
    api_key = environ["API_KEY"]
else: 
    print("API_KEY environment-variable not set!")

ambassadorDataCached = {}
ambassadorDataCached['ULDB'] = {}
def getDbCredentials():
    tenant_id = lib.get_tenant_id()

    if tenant_id is None:
        return {}

    if "ULDB" in ambassadorDataCached and tenant_id in ambassadorDataCached['ULDB']:
        return ambassadorDataCached['ULDB'][tenant_id]

    url = ambassador_url + '/' + ambassador_version + '/tenant/' + tenant_id + '/credentials/ULDB/'
    r = requests.get(url, params={'apikey': api_key, 'format': 'json'})

    if r.status_code != 200:
        return {}

    data = r.json()

    host = data['1st'] if '1st' in data else data['2nd'] if '2nd' in data else None
    username = data['usr'] if 'usr' in data else None
    password = data['pwd'] if 'pwd' in data else None
    database = data['dbn'] if 'dbn' in data else None

    if host is None or username is None or database is None:
        return {}

    result = {'server': host, 'user': username, 'password': password, 'database': database}
    ambassadorDataCached['ULDB'][tenant_id] = result
    print(result)
    return result

def getAmqpData():
    if "amqp" in ambassadorDataCached:
        return ambassadorDataCached['amqp']
    
    print("NEED TO IMPLEMENT FETCHING FROM AMBASSADOR!!!DATA IS JUST MOCKED")
    #url = ambassador_url + '/' + ambassador_version + '/tenant/' + tenant_id + '/credentials/AMQP/'
    #r = requests.get(url, params={'apikey': api_key, 'format': 'json'})

    #if r.status_code != 200:
    #    return {}

    #data = r.json()

    host = "lagos.netplace.matrix-computer.com" #data['1st'] if '1st' in data else data['2nd'] if '2nd' in data else None
    username = "adc" #data['usr'] if 'usr' in data else None
    password = "zeus4711" #data['pwd'] if 'pwd' in data else None
    port = "5672" #data['dbn'] if 'dbn' in data else None
    vhost = "/" #data['dbn'] if 'dbn' in data else None

    if host is None or username is None or password is None:
        return {}

    result = {'host': host, 'user': username, 'password': password, 'port': port, 'vhost':vhost}
    ambassadorDataCached['amqp'] = result

    return result

