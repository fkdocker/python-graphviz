from os import environ
import sys
from flask import request
import requests


def selfcheck():
    if "RESOURCE_AMBASSADOR" not in environ:
        print('Ambassador environment-variable not set!')
        #sys.exit("Ambassador environment-variable not set!")

    if "API_KEY" not in environ:
        print("API_KEY environment-variable not set!")
        #sys.exit("API_KEY environment-variable not set!")

    if "RESOURCE_AUTHORIZATION" not in environ:
        print("Environment-Variable RESOURCE_AUTHORIZATION not set")
        #sys.exit("Environment-Variable RESOURCE_AUTHORIZATION not set")

def generateId():
    dbconfig = ambassador()

    with open("sql/mssql/SELECT-NEWID.sql", "r") as query_file:
        query = query_file.read()

    with UseSQLServer(dbconfig) as cursor:
        cursor.execute(query)
        row = cursor.fetchone()

        new_id = row["ID"]

    return new_id


def compare(original_records, new_records, element_key):
    if element_key in new_records and new_records[element_key] != original_records[element_key]:
        return new_records[element_key]
    else:
        return original_records[element_key]


def dict_delta(original: dict, new: dict) -> dict:
    original_keys = set(original.keys())
    new_keys = set(new.keys())
    intersection = original_keys & new_keys
    changed = {}

    for key in intersection:
        if new[key] != original[key]:
            changed[key] = new[key]

    return changed


def get_tenant_id():
    if request.headers.get('X-JWT-Upline-Tenant') is not None:
        return request.headers.get('X-JWT-Upline-Tenant')

    return request.args.get('tenantId')

def multiply(a,b):
    return a * b;