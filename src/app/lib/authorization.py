from flask import request
from os import environ
from lib import lib
import requests

lib.selfcheck()

if "RESOURCE_AUTHORIZATION" in environ:
    authorization_url = environ["RESOURCE_AUTHORIZATION"]
    print("Using authorization-service " + authorization_url)
else:
    print("Environment-Variable RESOURCE_AUTHORIZATION not set")
    

def authorized(statuscode = 200, responsetext = '1'):
    tenant_id = lib.get_tenant_id()
    print("Call to authorization-service started")
    if tenant_id is None:
        return {}

    url = authorization_url + '/banana'
    r = requests.get(url, params={'tenantId': tenant_id, 'statuscode': statuscode, 'responsetext': responsetext})
    
    print("Call to authorization-service returned status " + str(r.status_code))

    if r.status_code == 200:
        return True
    else:
        return False
