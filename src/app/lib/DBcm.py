import pymssql

class ConnError(Exception):
    pass


class CredentialsError(Exception):
    pass


class SQLError(Exception):
    pass


class UseSQLServer:
    def __init__(self, dbconfig: dict) -> None:
        self.dbconfig = dbconfig
        self.dbsys = 'MSSQL'

    def __enter__(self) -> 'cursor':
        try:
            self.conn = pymssql.connect(**self.dbconfig, as_dict=True)
            self.cursor = self.conn.cursor()
            return self.cursor
        except pymssql.InterfaceError as err:
            raise ConnError(err)
        except pymssql.ProgrammingError as err:
            raise CredentialsError(err)

    def __exit__(self, exc_type, exc_value, exc_trace) -> None:
        self.conn.commit()
        self.cursor.close()
        self.conn.close()

        if exc_type is pymssql.ProgrammingError:
            raise SQLError(exc_value)
        elif exc_type:
            raise exc_type(exc_value)

    def getSys(self):
        return self.dbsys