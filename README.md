# Upline template Service
## Overview
This application is the template for our services

##Usage
Before the service will start, you'll have to add an API-Key in the "run.bat". If it's a new service you'll have to create one. Further Information can be found here:
https://gitlab.internal.matrix-computer.com/environment/secrets

To build the image, just call build.bat from it's directory. It'll automatically gives the image a name coresponding to the folder-name the whole projects is in. 

Once the build finished successfull, call run.bat from it's directory. This one will automatically start the container named like the folder-name the whole project exists in, sets environment variables etc.

The app performs a small selftest for some mandatory environment variables so if you haven't added an Api-Key yet, you'll be beaten with a error mesage.
Once the container runs, check whether the following URI's return results:
- http://localhost:8000/ (Simple example route)
- http://localhost:8000/docs/ (Swagger documentation with some default entries. Try out the default gets whether they work)
- http://localhost:8000/health (Health-check route, should return JSON with message "OK" and status 200)

## Contents
- Python Service (dockerized)
- Swagger-UI with "contract.yml"

## To Do
- [ ] Create Behat-Tests
- [ ] Check whether default swagger-contract has working OAuth settings
