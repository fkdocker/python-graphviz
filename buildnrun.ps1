
"Build container..."
docker build -t upline/template-service .
"Stop old container (if any) .."
docker stop $(docker ps -a -q --filter="name=uas")
"Remove old container (if any) .."
docker rm $(docker ps -a -q --filter="name=uas")
"Run newly build container..."
$pwd = (Resolve-Path .\).Path
"Path: $pwd"
$ip = (Get-NetIPAddress | ?{ $_.AddressFamily -eq "IPv4"  -and ($_.IPAddress -match "10.9") }).IPAddress

docker run --name uas -i -p 82:80 `
    -v $pwd/app:/app `
    -e "RESOURCE_AMBASSADOR=http://lb.internal.matrix-computer.com/resource-ambassador" `
    -e "RESOURCE_AUTHORIZATION=http://lb.internal.matrix-computer.com/upline-authorization-read" `
    -e "DEV=1" `
    upline/template-service `
    /app/server.py